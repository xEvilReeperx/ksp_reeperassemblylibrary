﻿using System;
using System.Reflection;

namespace ReeperAssemblyLibrary
{
    public interface IReeperAssemblyResolver
    {
        Assembly Resolve(object sender, ResolveEventArgs args);
    }
}

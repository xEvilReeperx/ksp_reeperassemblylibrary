﻿namespace ReeperAssemblyLibrary
{
    public interface IReeperAssemblyLoader
    {
        ILoadedAssemblyHandle Load(ReeperAssembly assembly);
    }
}
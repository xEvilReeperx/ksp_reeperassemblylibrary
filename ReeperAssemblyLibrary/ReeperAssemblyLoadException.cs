﻿using System;

namespace ReeperAssemblyLibrary
{
    public class ReeperAssemblyLoadException : Exception
    {
        public readonly ReeperAssembly Target;

        public ReeperAssemblyLoadException(ReeperAssembly reeper)
            : base("Failed to load ReeperAssembly " + reeper.File.name)
        {
            if (reeper == null) throw new ArgumentNullException("reeper");

            Target = reeper;
        }

        public ReeperAssemblyLoadException(string message) : base(message)
        {
            
        }

        public ReeperAssemblyLoadException(string message, Exception inner)
            : base(message, inner)
        {
            
        }
    }
}

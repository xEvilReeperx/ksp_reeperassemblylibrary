﻿namespace ReeperAssemblyLibrary
{
    public interface IRawAssemblyDataFactory
    {
        RawAssemblyData Create(ReeperAssembly assembly);
    }
}

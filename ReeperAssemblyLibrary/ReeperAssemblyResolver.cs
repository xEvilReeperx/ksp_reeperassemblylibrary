﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Mono.Cecil;
using ReeperCommon.Containers;
using ReeperCommon.Extensions;

namespace ReeperAssemblyLibrary
{
    /// <summary>
    /// This simple resolver will try to resolve missing assemblies by checking the AssemblyLoader
    /// list
    /// </summary>
    public class ReeperAssemblyResolver : IReeperAssemblyResolver
    {
        public Assembly Resolve(object sender, ResolveEventArgs args)
        {
            return AssemblyLoader.loadedAssemblies.FirstOrDefault(la => la.assembly.GetName().Name == args.Name)
                .With(la => la.assembly);
        }
    }
}

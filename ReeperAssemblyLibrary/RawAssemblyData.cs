﻿using System;
using System.IO;
using ReeperCommon.Containers;

namespace ReeperAssemblyLibrary
{
    public class RawAssemblyData
    {
        public ReeperAssembly ReeperAssembly { get; set; }
        public MemoryStream RawAssembly { get; set; }
        public Maybe<MemoryStream> SymbolStore { get; set; }


        public RawAssemblyData(ReeperAssembly reeperAssembly, MemoryStream rawAssembly, Maybe<MemoryStream> symbolStore)
        {
            if (reeperAssembly == null) throw new ArgumentNullException("reeperAssembly");

            ReeperAssembly = reeperAssembly;
            RawAssembly = rawAssembly;
            SymbolStore = symbolStore;
        }
    }
}

﻿namespace ReeperAssemblyLibrary
{
    public interface ILoadedAssemblyHandle
    {
        AssemblyLoader.LoadedAssembly LoadedAssembly { get; }
    }
}

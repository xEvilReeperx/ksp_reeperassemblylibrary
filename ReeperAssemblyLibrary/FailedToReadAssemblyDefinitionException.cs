﻿using System;

namespace ReeperAssemblyLibrary
{
    public class FailedToReadAssemblyDefinitionException : Exception
    {
        public FailedToReadAssemblyDefinitionException() : base("Failed to read assembly definition")
        {
            
        }

        public FailedToReadAssemblyDefinitionException(string message) : base(message)
        {
            
        }

        public FailedToReadAssemblyDefinitionException(string message, Exception inner) : base(message, inner)
        {
            
        }

        public FailedToReadAssemblyDefinitionException(ReeperAssembly assembly)
            : base("Failed to read assembly definition from " + assembly.File.fullPath)
        {
            
        }
    }
}

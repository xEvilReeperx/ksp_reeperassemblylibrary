﻿using System;
using System.Linq;
using ReeperCommon.Containers;

namespace ReeperAssemblyLibrary
{
    public class ReeperAssembly
    {
        public ReeperAssembly(UrlDir.UrlFile file, Maybe<UrlDir.UrlFile> symbols, Maybe<UrlDir.UrlFile> configFile)
        {
            if (file == null) throw new ArgumentNullException("file");
            if (file.fileExtension == ".dll")
                throw new InvalidExtensionException(file.fileExtension);

            File = file;
            Symbols = symbols;

            if (configFile.Any())
                Config = ConfigNode.Load(configFile.Single().fullPath).ToMaybe();
        }


        public UrlDir.UrlFile File { get; private set; }
        public Maybe<UrlDir.UrlFile> Symbols { get; private set; }
        public Maybe<ConfigNode> Config { get; private set; }

        public override string ToString()
        {
            return string.Format("ReeperAssembly: {0} @ {1}, symbols {2}, config {3}", 
                File.name,
                File.url,
                (Symbols.Any() ? "YES" : "NO"), 
                (Config.Any() ? "YES" : "NO"));
        }
    }
}

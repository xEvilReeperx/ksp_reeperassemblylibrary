﻿using System.Reflection;

namespace ReeperAssemblyLibrary
{
    public interface ILoadedAssemblyInstaller
    {
        ILoadedAssemblyHandle Install(Assembly assembly, ReeperAssembly source);
    }
}

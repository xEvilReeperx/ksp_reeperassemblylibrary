﻿using System;

namespace ReeperAssemblyLibrary
{
    public class InvalidExtensionException : Exception
    {
        public InvalidExtensionException() : base("Invalid assembly extension for ReeperAssembly")
        {
            
        }

        public InvalidExtensionException(string badExtension)
            : base("The value " + badExtension + " is not the correct extension")
        {
            
        }

        public InvalidExtensionException(string message, Exception innerException) : base(message, innerException)
        {
            
        }
    }
}

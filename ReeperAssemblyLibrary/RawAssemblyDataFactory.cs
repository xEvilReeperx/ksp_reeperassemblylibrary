﻿using System;
using System.IO;
using System.Linq;
using ReeperCommon.Containers;

namespace ReeperAssemblyLibrary
{
    public class RawAssemblyDataFactory : IRawAssemblyDataFactory
    {
        public RawAssemblyData Create(ReeperAssembly assembly)
        {
            if (assembly == null) throw new ArgumentNullException("assembly");

            if (!File.Exists(assembly.File.fullPath))
                throw new FileNotFoundException("File not found", assembly.File.fullPath);

            if (assembly.Symbols.Any())
                if (!File.Exists(assembly.Symbols.Single().fullPath))
                    throw new FileNotFoundException("Symbols not found", assembly.Symbols.Single().fullPath);

            return new RawAssemblyData(assembly, ReadRawAssembly(assembly.File.fullPath), 
                assembly.Symbols
                .SingleOrDefault()
                .Return(urf => ReadRawSymbolStore(urf.fullPath)
                    .Return(ms => ms.Length > 0 ? Maybe<MemoryStream>.With(ms) : Maybe<MemoryStream>.None, Maybe<MemoryStream>.None), 
                    Maybe<MemoryStream>.None));
        }


        private static MemoryStream ReadRawAssembly(string fullPath)
        {
            return new MemoryStream(File.ReadAllBytes(fullPath));
        }


        private static MemoryStream ReadRawSymbolStore(string fullPath)
        {
            return new MemoryStream(File.ReadAllBytes(fullPath));
        }
    }
}

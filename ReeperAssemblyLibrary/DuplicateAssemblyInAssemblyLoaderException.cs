﻿using System;

namespace ReeperAssemblyLibrary
{
    public class DuplicateAssemblyInAssemblyLoaderException : Exception
    {
        public DuplicateAssemblyInAssemblyLoaderException()
            : base("Duplicate ReeperAssembly has already been loaded by AssemblyLoader")
        {
            
        }

        public DuplicateAssemblyInAssemblyLoaderException(string message) : base(message)
        {
            
        }

        public DuplicateAssemblyInAssemblyLoaderException(ReeperAssembly reeper)
            : base(reeper.File.name + " has already been loaded by AssemblyLoader")
        {
            
        }

        public DuplicateAssemblyInAssemblyLoaderException(string message, Exception inner) : base(message, inner)
        {
            
        }
    }
}

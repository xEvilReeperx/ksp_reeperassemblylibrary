﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ReeperAssemblyLibrary
{
    ///
    /// Inserts an Assembly loaded manually into AssemblyLoader's LoadedAssemblyList
    /// 
// ReSharper disable once ClassNeverInstantiated.Global
    public class LoadedAssemblyInstaller : ILoadedAssemblyInstaller
    {
        public ILoadedAssemblyHandle Install(Assembly assembly, ReeperAssembly source)
        {
            if (assembly == null) throw new ArgumentNullException("assembly");
            if (source == null) throw new ArgumentNullException("source");


            if (AssemblyLoader.loadedAssemblies.Any(l => l.url == source.File.url))
                throw new DuplicateAssemblyInAssemblyLoaderException(source);

            var la = new AssemblyLoader.LoadedAssembly(assembly, source.File.fullPath, source.File.parent.url,
                source.Config.SingleOrDefault());

            InstallTypes(la);

            AssemblyLoader.loadedAssemblies.Add(la);

            return new LoadedAssemblyHandle(la);
        }


        // The game looks inside this type list for certain types; we need to make sure they're there to be found
        private static void InstallTypes(AssemblyLoader.LoadedAssembly loadedAssembly)
        {
            InsertTypeListIntoLoadedAssembly<Part>(loadedAssembly, GetTypes<Part>(loadedAssembly.assembly).ToList());

            InsertTypeListIntoLoadedAssembly<InternalModule>(loadedAssembly,
                GetTypes<InternalModule>(loadedAssembly.assembly).ToList());

            InsertTypeListIntoLoadedAssembly<PartModule>(loadedAssembly, GetTypes<PartModule>(loadedAssembly.assembly).ToList());
            InsertTypeListIntoLoadedAssembly<ScenarioModule>(loadedAssembly, GetTypes<ScenarioModule>(loadedAssembly.assembly).ToList());
        }



        private static void InsertTypeListIntoLoadedAssembly<TKey>(
            AssemblyLoader.LoadedAssembly loadedAssembly,
            List<Type> typesToInsert)
        {
            if (loadedAssembly == null) throw new ArgumentNullException("loadedAssembly");
            if (typesToInsert == null) throw new ArgumentNullException("typesToInsert");

            List<Type> loadedTypes;

            if (!loadedAssembly.types.TryGetValue(typeof(TKey), out loadedTypes))
                loadedAssembly.types.Add(typeof(TKey), typesToInsert);
            else loadedTypes.AddRange(typesToInsert);
        }


        private static IEnumerable<Type> GetTypes<T>(Assembly assembly)
        {
            return assembly.GetTypes().Where(ty => ty.IsSubclassOf(typeof(T)) && !ty.IsAbstract && !ty.IsGenericType);
        }
    }
}

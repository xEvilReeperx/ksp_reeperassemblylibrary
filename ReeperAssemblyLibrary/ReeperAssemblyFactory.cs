﻿using System;
using System.Linq;
using ReeperCommon.Containers;

namespace ReeperAssemblyLibrary
{
// ReSharper disable once ClassNeverInstantiated.Global
    public class ReeperAssemblyFactory : IReeperAssemblyFactory
    {
        private readonly string _extensionName;
        private readonly string _symbolExtensionName;


        public ReeperAssemblyFactory(string extensionName, string symbolExtensionName)
        {
            if (extensionName == null) throw new ArgumentNullException("extensionName");
            if (symbolExtensionName == null) throw new ArgumentNullException("symbolExtensionName");

            _extensionName = extensionName;
            _symbolExtensionName = symbolExtensionName;
        }


        public ReeperAssembly Create(UrlDir.UrlFile reeperAssemblyFile)
        {
            if (reeperAssemblyFile == null) throw new ArgumentNullException("reeperAssemblyFile");
            if (reeperAssemblyFile.fileExtension != _extensionName)
                throw new InvalidExtensionException(reeperAssemblyFile.fileExtension);

            return new ReeperAssembly(reeperAssemblyFile, 
                GetSymbolFile(reeperAssemblyFile), 
                GetConfigFile(reeperAssemblyFile));
        }


        private Maybe<UrlDir.UrlFile> GetSymbolFile(UrlDir.UrlFile assemblyFile)
        {
            return assemblyFile.parent.files
                .FirstOrDefault(f => f.name == (assemblyFile.name + "." + assemblyFile.fileExtension) && f.fileExtension == _symbolExtensionName).ToMaybe();
        }


        private Maybe<UrlDir.UrlFile> GetConfigFile(UrlDir.UrlFile assemblyFile)
        {
            // Assembly.reeper.config
            // Url of above excludes filename, so:
            return assemblyFile.parent.GetFile(assemblyFile.url + "." + _extensionName).ToMaybe();
        }
    }
}

﻿using System;
using System.CodeDom;
using System.Linq;
using System.Reflection;
using ReeperCommon.Containers;
using ReeperCommon.Logging;

namespace ReeperAssemblyLibrary
{
    /// <summary>
    /// Input: ReeperAssemblies (renamed DLL file on disk), Output: assemblies are loaded into memory
    /// and installed
    /// </summary>
// ReSharper disable once ClassNeverInstantiated.Global
    public class ReeperAssemblyLoader : IReeperAssemblyLoader
    {
        private readonly ILoadedAssemblyInstaller _installer;
        private readonly IRawAssemblyDataFactory _rawAssemblyFactory;
        protected readonly ILog Log;

        public ReeperAssemblyLoader(
            ILoadedAssemblyInstaller installer, 
            IRawAssemblyDataFactory rawAssemblyFactory,
            ILog log)
        {
            if (installer == null) throw new ArgumentNullException("installer");
            if (rawAssemblyFactory == null) throw new ArgumentNullException("rawAssemblyFactory");
            if (log == null) throw new ArgumentNullException("log");

            _installer = installer;
            _rawAssemblyFactory = rawAssemblyFactory;
            Log = log;
        }


        public ILoadedAssemblyHandle Load(ReeperAssembly assembly)
        {
            if (assembly == null) throw new ArgumentNullException("assembly");

            var result = LoadAssembly(assembly);

            if (result.Any()) return _installer.Install(result.Single(), assembly);

            Log.Error("Failed to load assembly " + assembly.File.fullPath);
            throw new ReeperAssemblyLoadException(assembly);
        }



        protected virtual Maybe<Assembly> LoadAssembly(ReeperAssembly assembly)
        {
            Log.Normal("Loading " + assembly.File.name +
                assembly.Symbols.SingleOrDefault().Return(f => " with symbols", " without symbols"));

            var raw = _rawAssemblyFactory.Create(assembly);
            Assembly result;

            try
            {
                result = raw.SymbolStore.Any()
                    ? Assembly.Load(raw.RawAssembly.ToArray(), raw.SymbolStore.Single().ToArray())
                    : Assembly.Load(raw.RawAssembly.ToArray());

            }
            catch (Exception e)
            {
                Log.Error("Exception while loading " + assembly + ": " + e);
                throw;
            }


            if (result == null) return Maybe<Assembly>.None;

            // some assemblies might have their own dependencies embedded within them, so
            // if they have a handler created by Costura, set it up now
            Log.Debug("Checking for Costura module initializer...");
            var costuraLoader = result.GetType("Costura.AssemblyLoader").ToMaybe();

            if (costuraLoader.Any())
            {
                Log.Debug("Found and invoking");
                costuraLoader.Single().GetMethod("Attach", BindingFlags.Public | BindingFlags.Static)
                    .Invoke(null, null);
            }

            return result.ToMaybe();
        }
    }
}

﻿namespace ReeperAssemblyLibrary
{
    public interface IReeperAssemblyFactory
    {
        ReeperAssembly Create(UrlDir.UrlFile file);
    }
}
